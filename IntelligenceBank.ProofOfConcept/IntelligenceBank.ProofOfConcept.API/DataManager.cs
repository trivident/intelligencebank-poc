﻿using System;
using System.Linq;
using IntelligenceBank.ProofOfConcept.Models;
using IntelligenceBank.ProofOfConcept.Models.RestClient;

namespace IntelligenceBank.ProofOfConcept.API
{
    public class DataManager
    {
        private readonly ApiClient _ibClient;
        private SecurityToken _securityToken;
        private readonly AccountSettings _accountSettings;
        public DataManager(AccountSettings accountSettings)
        {
            _ibClient = new ApiClient(accountSettings);
            _accountSettings = accountSettings;
        }

        public ResourceResponse GetResource(RequestParameter parameters)
        {
            return _ibClient.GetResource(SecurityToken, parameters);
        }

        public ResourceEstimatedSize GetEstimatedFileSize(RequestParameter parameters)
        {
            return _ibClient.GetEstimateSize(SecurityToken, parameters);
        }

        public FolderContent GetFolderContent(String folderId)
        {
            var iFolder = _ibClient.GetFolder(SecurityToken, folderId);

            FolderContent folderContent = null;

            if (iFolder != null)
            {
                folderContent = new FolderContent
                {
                    IsNeedToDelete = iFolder.IsNeedtoDelete,
                    Id = folderId,
                    Folders = iFolder.Folders != null ? iFolder.Folders.Select(MapFolder) : Enumerable.Empty<Folder>(),
                    Resources = iFolder.Resources != null ? iFolder.Resources.Select(MapResource) : Enumerable.Empty<Resource>()
                };
            }
            return folderContent;
        }

        public string GetThumbnailUrl(string resourceId, string extention)
        {
            return _ibClient.GetResourceThumbnailUrl(SecurityToken, resourceId, extention);
        }

        private Resource MapResource(ResourceModel resourceModel)
        {
            var resource = new Resource();
            resource.Id = resourceModel.ResourceId;
            resource.Name = resourceModel.Title;
            resource.FileType = resourceModel.FileType;
            resource.Description = resourceModel.Description;
            if (resourceModel.Versions != null)
            {
                var versionModel = resourceModel.Versions.LastOrDefault();

                if (versionModel != null)
                {
                    resource.CreationDate = versionModel.CreationDate;
                }
            }
            resource.FileSize = resourceModel.FileSize;
            resource.OriginalName = resourceModel.FileName;
            resource.ResourceDate = resourceModel.ResourceDate;
            resource.UpdatedDate = resourceModel.UpdateDate;
            resource.Tags = resourceModel.Tags;
            if (resourceModel.KeyWords != null && resourceModel.KeyWords.Count > 0)
                resource.Keywords = String.Join(",", resourceModel.KeyWords);
            resource.OwnershipType = resourceModel.OwnershipType;
            resource.Owner = resourceModel.Owner;
            resource.UsageInfo = resourceModel.UsageDetails;
            resource.Width = resourceModel.Width;
            resource.Height = resourceModel.Height;
            resource.ReviewDate = resourceModel.ReviewDate;
            resource.ColourSpace = resourceModel.ColourSpace;
            resource.PublicUrl = resourceModel.PublicUrl;
            resource.Version = resourceModel.Version;
            //resource.ImageThumbnail = GetThumbnailUrl(resource.Id, resource.FileType);
            return resource;
        }

        private Folder MapFolder(FolderModel folderModel)
        {
            var folder = new Folder();
            folder.Id = folderModel.FolderId;
            folder.Name = folderModel.Name;
            return folder;
        }


        private SecurityToken SecurityToken
        {
            get
            {
                if (_securityToken == null)
                {
                    //todo: add logic to check if token is valid
                    String sessionToken;
                    var loginResponse = _ibClient.Login(out sessionToken);
                    if (loginResponse == null)
                        throw new ApplicationException("IntelligenceBank API server is not available");
                    if (loginResponse.ApiKey == null)
                        throw new ApplicationException(loginResponse.Message);


                    _securityToken = new SecurityToken()
                    {
                        ApiKey = loginResponse.ApiKey,
                        ApiServerUrl = _accountSettings.ApiServerUrl,
                        SessionToken = sessionToken,
                        UserId = loginResponse.ClientUUId
                    };


                }
                return _securityToken;
            }
        }

        public PresetsModel GetPresets()
        {
            PresetsModel presets = _ibClient.GetPresets(SecurityToken);
            return presets;
        }
    }
}
