﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;

namespace IntelligenceBank.ProofOfConcept.API
{
    public class AccountSettings
    {
        public const string AccountTemplateName = "IB Account";

        public string ApiServerUrl { get; set; }

        public string PlarformUrl { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }


        public AccountSettings()
        {
            var accountSettingItem = ConfigurationManager.AppSettings;
            if (accountSettingItem != null)
            {
                ApiServerUrl = accountSettingItem["api-server-url"];
                PlarformUrl = accountSettingItem["platform-url"];
                var match = Regex.Match(PlarformUrl, "^(http(s)?://)?(?<host>[^/]+).*$", RegexOptions.IgnoreCase);
                if (match.Success && match.Groups["host"] != null)
                {
                    PlarformUrl = match.Groups["host"].Value;
                }
                Username = accountSettingItem["username"];
                Password = accountSettingItem["password"];
            }
        }

    }
}
