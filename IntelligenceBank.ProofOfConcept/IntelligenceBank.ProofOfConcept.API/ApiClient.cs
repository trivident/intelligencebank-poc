﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using IntelligenceBank.ProofOfConcept.Models;
using IntelligenceBank.ProofOfConcept.Models.RestClient;
using RestSharp;

namespace IntelligenceBank.ProofOfConcept.API
{
    public class ApiClient
    {
        private const String TokenCookieName = "_aid";
        private readonly AccountSettings _accountSettings;
        private readonly RestClient _client;
        private readonly CookieContainer _cookieContainer;

        public ApiClient(AccountSettings accountSettings)
        {
            if (!string.IsNullOrEmpty(accountSettings.ApiServerUrl) && !accountSettings.ApiServerUrl.ToLower().StartsWith("http"))
            {
                accountSettings.ApiServerUrl = "http://" + accountSettings.ApiServerUrl;
            }

            _accountSettings = accountSettings;
            _client = new RestClient(accountSettings.ApiServerUrl) { FollowRedirects = false };
            _cookieContainer = new CookieContainer();
            _client.CookieContainer = _cookieContainer;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;
        }

        public LoginResponse Login(out String sessionToken)
        {
            sessionToken = null;

            var request = new RestRequest("webapp/1.0/login", Method.POST);
            request.AddParameter("p70", _accountSettings.Username);
            request.AddParameter("p80", _accountSettings.Password);
            request.AddParameter("p90", _accountSettings.PlarformUrl);

            //Timer.Start();
            var response = _client.Execute<LoginResponse>(request);
            //Timer.Stop();

            if (response.Data != null)
            {
                var sessionCookie = _cookieContainer.GetCookies(response.ResponseUri)[TokenCookieName];

                if (sessionCookie != null)
                    sessionToken = sessionCookie.Value;
            }



            return response.Data;
        }

        public FolderContentModel GetFolder(SecurityToken token)
        {
            return GetFolder(token, null);
        }

        private RestRequest CreateRequest(string resource, SecurityToken token)
        {
            var request = new RestRequest(resource, Method.GET);
            request.AddParameter("p10", token.ApiKey);
            request.AddParameter("p20", token.UserId);

            var sessionCookie = new Cookie(TokenCookieName, token.SessionToken)
            {
                Domain = _client.BaseUrl.Host
            };
            _cookieContainer.Add(sessionCookie);
            return request;
        }

        public FolderContentModel GetFolder(SecurityToken token, String folderId)
        {

            var request = CreateRequest("webapp/1.0/resources", token);
            request.Timeout = 18000000;

            string url = string.Format("{0}{1}?p10={2}&p20={3}", token.ApiServerUrl, request.Resource, token.ApiKey, token.UserId);

            if (!String.IsNullOrEmpty(folderId))
            {
                request.AddParameter("folderuuid", folderId);
                url += string.Format("&folderuuid={0}", folderId);
            }

            string logMessage = string.Format("{0} {1}:{2}", url, TokenCookieName, token.SessionToken);
            //Log.Info(logMessage, typeof(ApiClient));

            //Timer.Start();
            var response = _client.Execute<FolderContentResponse>(request);
            //Timer.Stop();
            var result = new FolderContentModel() { IsNeedtoDelete = false };

            if (response.Data != null)
            {
                if (!string.IsNullOrEmpty(response.Data.Message))
                {
                    if (response.Data.Message.ToLower().Contains("a server error occurred"))
                    {
                        result.IsNeedtoDelete = true;
                        return result;
                    }
                    return result;
                }

                result.IsNeedtoDelete = null;
                if (response.Data.Response != null)
                {
                    result.Folders = response.Data.Response.Folders;
                    result.HasVersions = response.Data.Response.HasVersions;
                    result.Resources = response.Data.Response.Resources;
                }
                return result;
            }

            //Log.Error(response.ErrorMessage, response.ErrorException, typeof(ApiClient));

            return result;
        }

        public ResourceResponse GetResource(SecurityToken token, RequestParameter parameters)
        {
            return GetRemoteResource(token, parameters);
        }

        private ResourceResponse GetRemoteResource(SecurityToken token, RequestParameter parameters)
        {
            var request = CreateRequest("webapp/1.0/resources", token);
            request.Timeout = 18000000;

            string url = string.Format("{0}{1}?p10={2}&p20={3}", token.ApiServerUrl, request.Resource, token.ApiKey, token.UserId);
            string value = string.Empty;
            string paramName = string.Empty;

            if (parameters.IsPreview)
            {
                value = parameters.IsPreview.ToString().ToLower();
                paramName = "preview";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }

            if (!string.IsNullOrEmpty(parameters.ResourceId))
            {
                paramName = "fileuuid";
                url += string.Format("&{0}={1}", paramName, parameters.ResourceId);
                request.AddParameter(paramName, parameters.ResourceId);
            }

            if (!string.IsNullOrEmpty(parameters.Ext))
            {
                value = parameters.Ext.ToLower();
                paramName = "ext";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }

            if (!string.IsNullOrEmpty(parameters.Size))
            {
                paramName = "size";
                url += string.Format("&{0}={1}", paramName, parameters.Size);
                request.AddParameter(paramName, parameters.Size);
            }

            if (parameters.CropW > 0)
            {
                paramName = "cropwidth";
                url += string.Format("&{0}={1}", paramName, parameters.CropW);
                request.AddParameter(paramName, parameters.CropW);
            }

            if (parameters.CropH > 0)
            {
                paramName = "cropheight";
                url += string.Format("&{0}={1}", paramName, parameters.CropH);
                request.AddParameter(paramName, parameters.CropH);
            }

            if (parameters.CropX >= 0)
            {
                paramName = "cropx";
                url += string.Format("&{0}={1}", paramName, parameters.CropX.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture));
                request.AddParameter(paramName, parameters.CropX);
            }

            if (parameters.CropY >= 0)
            {
                paramName = "cropy";
                url += string.Format("&{0}={1}", paramName, parameters.CropY.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture));
                request.AddParameter(paramName, parameters.CropY);
            }

            if ((int)parameters.CompressionType > 0)
            {
                value = ((int)parameters.CompressionType).ToString();
                paramName = "compressiontype";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }

            if ((int)parameters.CompressionQuality > 0)
            {
                value = ((int)parameters.CompressionQuality).ToString();
                paramName = "compression";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }
#if DEBUG
            string logMessage = string.Format("{0} {1}:{2}", url, TokenCookieName, token.SessionToken);
            //Log.Info(logMessage, typeof(ApiClient));
#endif

            var response = _client.DownloadData(request);

            var resourceResponse = new ResourceResponse
            {
                ResourceStream = new MemoryStream(response)
            };

            return resourceResponse;
        }

        public ResourceEstimatedSize GetEstimateSize(SecurityToken token, RequestParameter parameters)
        {
            var request = CreateRequest("webapp/1.0/resources", token);
            request.Timeout = 18000000;

            string url = string.Format("{0}{1}?p10={2}&p20={3}", token.ApiServerUrl, request.Resource, token.ApiKey, token.UserId);
            string value = string.Empty;
            string paramName = string.Empty;

            if (parameters.IsPreview)
            {
                value = parameters.IsPreview.ToString().ToLower();
                paramName = "preview";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }

            if (!string.IsNullOrEmpty(parameters.ResourceId))
            {
                paramName = "fileuuid";
                url += string.Format("&{0}={1}", paramName, parameters.ResourceId);
                request.AddParameter(paramName, parameters.ResourceId);
            }

            if (!string.IsNullOrEmpty(parameters.Ext))
            {
                value = parameters.Ext.ToLower();
                paramName = "ext";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }

            if (!string.IsNullOrEmpty(parameters.Size))
            {
                paramName = "size";
                url += string.Format("&{0}={1}", paramName, parameters.Size);
                request.AddParameter(paramName, parameters.Size);
            }

            if (parameters.CropW > 0)
            {
                paramName = "cropwidth";
                url += string.Format("&{0}={1}", paramName, parameters.CropW);
                request.AddParameter(paramName, parameters.CropW);
            }

            if (parameters.CropH > 0)
            {
                paramName = "cropheight";
                url += string.Format("&{0}={1}", paramName, parameters.CropH);
                request.AddParameter(paramName, parameters.CropH);
            }

            if (parameters.CropX >= 0)
            {
                paramName = "cropx";
                url += string.Format("&{0}={1}", paramName, parameters.CropX);
                request.AddParameter(paramName, parameters.CropX);
            }

            if (parameters.CropY >= 0)
            {
                paramName = "cropy";
                url += string.Format("&{0}={1}", paramName, parameters.CropY);
                request.AddParameter(paramName, parameters.CropY);
            }

            if ((int)parameters.CompressionType > 0)
            {
                value = ((int)parameters.CompressionType).ToString();
                paramName = "compressiontype";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }

            if ((int)parameters.CompressionQuality > 0)
            {
                value = ((int)parameters.CompressionQuality).ToString();
                paramName = "compression";
                url += string.Format("&{0}={1}", paramName, value);
                request.AddParameter(paramName, value);
            }
            request.AddParameter("fileinfo", "true");
            url += string.Format("&{0}={1}", "fileinfo", "true");
#if DEBUG
            string logMessage = string.Format("{0} {1}:{2}", url, TokenCookieName, token.SessionToken);
            //Log.Info(logMessage, typeof(ApiClient));
#endif

            var response = _client.Execute<ResourceEstimatedSize>(request);
            if (response == null || response.Data == null)
            {
                response = new RestResponse<ResourceEstimatedSize>
                {
                    Data = new ResourceEstimatedSize()
                    {
                        FileSize = "API server responded with error"
                    }
                };
            }
            return response.Data;
        }

        public String GetResourceThumbnailUrl(SecurityToken token, String resourceId, String extension)
        {
            var request = CreateRequest("webapp/1.0/icon", token);

            request.AddParameter("name", resourceId);
            request.AddParameter("type", "file");
            request.AddParameter("ext", extension);

            //Timer.Start();
            var response = _client.Execute(request);
            //Timer.Stop();

            String thumbnailUrl = String.Empty;

            if (response.StatusCode == HttpStatusCode.Found)
            {
                var locationHeader = response.Headers.SingleOrDefault(h => h.Type == ParameterType.HttpHeader && h.Name == "Location");
                if (locationHeader != null)
                    thumbnailUrl = locationHeader.Value.ToString();
            }
            return thumbnailUrl;
        }

        public PresetsModel GetPresets(SecurityToken token)
        {
            RestRequest request = CreateRequest("webapp/1.0/downloadpresets", token);
            request.Timeout = 18000000;
            var response = _client.Execute<PresetsModel>(request);

            if (response != null)
                return response.Data;

            return null;
        }

    }
}
