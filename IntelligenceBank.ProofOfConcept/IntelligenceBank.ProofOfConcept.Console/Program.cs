﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntelligenceBank.ProofOfConcept.API;
using IntelligenceBank.ProofOfConcept.Models;

namespace IntelligenceBank.ProofOfConcept.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var accountSettings = new AccountSettings();
            var dataManager = new DataManager(accountSettings);

            var folderContent = dataManager.GetFolderContent("454493839dfdedbd9e9afbbbb895dca7");
            System.Console.WriteLine("Found {0} resources in folder {1}", folderContent.Resources.Count(), folderContent.Id);
            foreach(var resource in folderContent.Resources)
            {
                System.Console.WriteLine("Name: {0}\t File type: {1}", resource.Name, resource.FileType);
            }
        }
    }
}
