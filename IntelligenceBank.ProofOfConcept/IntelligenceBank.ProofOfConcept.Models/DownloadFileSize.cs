﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class DownloadFileSize
    {
        public string Title { get; set; }
        public double Value { get; set; }
        public bool Custom { get; set; }
        public bool Original { get; set; }
    }
}
