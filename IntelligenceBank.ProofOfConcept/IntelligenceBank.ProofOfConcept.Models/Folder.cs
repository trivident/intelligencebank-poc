﻿using System;
using System.Collections.Generic;

namespace IntelligenceBank.ProofOfConcept.Models
{
    public class Folder
    {
        public string SitecoreId { get; set; }

        public String Id { get; set; }

        public String Name { get; set; }

        public IEnumerable<Resource> Resources { get; set; }

        public IEnumerable<Folder> Folders { get; set; }
    }
}
