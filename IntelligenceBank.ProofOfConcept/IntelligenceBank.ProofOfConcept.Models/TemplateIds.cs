﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class TemplateIds
    {
        public const string  IBFolder = "{7B047AEC-4048-4EC3-85D6-5DEAF3754664}";
        public const string IBAccount = "{13B01CFD-FA51-44A2-94CC-2B03131FF230}";
        public const string IBResource = "{447D3D2A-BF24-47B3-9C12-9F93DD7D940A}";
        public const string IBPreset = "{CBA5F15F-39C8-40D5-82C2-0EA05A32D273}";
        public const string IBPresetFolder = "{3FF4D616-AA3F-4603-BF00-7342FA106487}";
        public const string IBDropDownItem = "{B207C988-3779-4079-9B7E-8F31D8082025}";
    }
}
