﻿using System;
using System.Collections.Generic;

namespace IntelligenceBank.ProofOfConcept.Models
{
    public class Resource
    {
       public string SitecoreId { get; set; } 

        public String Id { get; set; }

        public String Name { get; set; }

        public String FileType { get; set; }

        public String Description { get; set; }

        public DateTime CreationDate { get; set; }

        public string FileSize { get; set; }

        public string OriginalName { get; set; }

        public DateTime ResourceDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public IEnumerable<string> Tags { get; set; }
        
        public string PreviewId { get; set; } 

        public string Keywords { get; set; }

        public string OwnershipType { get; set; }

        public string Owner { get; set; } //?

        public string UsageInfo { get; set; } //?

        public string Width { get; set; }
        
        public string Height { get; set; }

        public DateTime ReviewDate { get; set; }
        
        public string ColourSpace { get; set; }
        
        public int Version { get; set; }
        
        public string ImageThumbnail { get; set; }
        public string CropX { get;set; }
        public string CropY { get;set; }
        public string CropW { get; set; }
        public string CropH { get; set; }
        public string CopressionType { get; set; }
        public string CompressionQuality { get; set; }

        public string PublicUrl { get; set; }
    }
}


