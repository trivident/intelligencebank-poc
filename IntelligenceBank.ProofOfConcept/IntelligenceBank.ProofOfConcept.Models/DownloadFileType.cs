﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class DownloadFileType
    {
        public FileTypes Type { get; set; }
        public bool Original { get; set; }
        public string Title {
            get
            {
                if (Original)
                {
                    return string.Format("Original ({0})", Type);
                }
                return Type.ToString();
            }
        }
    }
}
