﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public static class TemplateNames
    {
        public const string MediaFolder = "Media folder";
        public const string MainSection  = "Main section";
        public const string IBAccount = "IB Account";
        public const string IBResource = "IB Resource";
        public const string IBFolder = "IB Folder";
    }
}
