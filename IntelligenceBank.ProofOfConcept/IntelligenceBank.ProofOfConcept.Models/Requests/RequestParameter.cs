﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class RequestParameter
    {
        public string ResourceId { get; set; }
        public CompressionType CompressionType { get; set; }
        public CompressionQuality CompressionQuality { get; set; }
        public double CropY { get; set; }
        public double CropX { get; set; }
        public int CropH { get; set; }
        public int CropW { get; set; }
        public string Size { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        private string _ext = string.Empty;

        public string Ext
        {
            get
            {
                if (!string.IsNullOrEmpty(_ext))
                    _ext = _ext.Trim().ToLower();

                return _ext;
            }
            set
            {
                _ext = value;
            }
        }

        public string Version { get; set; }
        public bool IsPreview { get; set; }
    }
}