﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public enum FileTypes
    {
        Other = 0,
        PNG,
        GIF,
        JPEG,
        JPG ,
        TIFF,
        TIF,
        BMP,
        PSD,
        ICO,
        EPS ,
    }
}
