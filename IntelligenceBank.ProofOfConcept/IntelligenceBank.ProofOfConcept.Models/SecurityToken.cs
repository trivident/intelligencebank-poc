﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class SecurityToken
    {
        public string SessionToken { get; set; }
        public string UserId { get; set; }
        public string ApiKey { get; set; }
        public string ApiServerUrl { get; set; }
    }
}
