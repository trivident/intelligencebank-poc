﻿using System;
using System.Collections.Generic;

namespace IntelligenceBank.ProofOfConcept.Models
{
    public class FolderContent
    {
        public bool? IsNeedToDelete { get; set; }

        public String Id { get; set; }

        public IEnumerable<Resource> Resources { get; set; }

        public IEnumerable<Folder> Folders { get; set; }
    }
}
