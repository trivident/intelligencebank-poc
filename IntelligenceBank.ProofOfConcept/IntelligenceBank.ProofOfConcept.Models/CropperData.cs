﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class CropperData
    {
        public string X { get; set; }
        public string Y { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
    }
}