﻿using System.ComponentModel.DataAnnotations;

namespace IntelligenceBank.ProofOfConcept.Models
{
    public enum CompressionQuality
    {
        [Display(Name = "Maximum", Description = "100%")]
        Maximum = 100,

        [Display(Name = "Very High", Description = "80%")]
        VeryHigh = 80,

        [Display(Name = "High", Description = "60%")]
        High = 60,

        [Display(Name = "Medium", Description = "30%")]
        Medium = 30,

        [Display(Name = "Low", Description = "10%")]
        Low = 10
    }
}