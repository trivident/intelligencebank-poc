﻿using System;
using System.Collections.Generic;
using RestSharp.Deserializers;

namespace IntelligenceBank.ProofOfConcept.Models.RestClient
{

    public class FolderContentResponse
    {
        public FolderContentModel Response { get; set; }
        public string Message { get; set; }
    }

    public class FolderContentModel
    {
        public bool? IsNeedtoDelete { get; set; }

        public string HasVersions { get; set; }

        [DeserializeAs(Name = "folder")]
        public List<FolderModel> Folders { get; set; }

        [DeserializeAs(Name = "resource")]
        public List<ResourceModel> Resources { get; set; }
    }

    public class FolderModel
    {
        [DeserializeAs(Name = "folderuuid")]
        public string FolderId { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }

        [DeserializeAs(Name = "CreatedTime")]
        public DateTime CreationDate { get; set; }

        public Int32 Comments { get; set; }

        [DeserializeAs(Name = "Perm")]
        public string Permission { get; set; }
        
        public FolderChildren Children { get; set; }

        public Int32 SortOrder { get; set; }

        public string DefaultOrder { get; set; }
    }


    public class FolderChildren
    {
        public Int32 Folders { get; set; }

        public Int32 Resources { get; set; }
    }

    public class ResourceModel
    {
        [DeserializeAs(Name = "resourceuuid")]
        public string ResourceId { get; set; }
        public string ResourceType { get; set; }
        public string Seen { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Sortorder { get; set; }

        [DeserializeAs(Name = "updatedat")]
        public DateTime UpdateDate { get; set; }
        [DeserializeAs(Name = "processedat")]
        public DateTime ProcesseDate { get; set; }
        public string UpdatedBy { get; set; }
        public string OwnershipType { get; set; }
        public List<string> Tags { get; set; }
        [DeserializeAs(Name = "fileuuid")]
        public string FileId { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        [DeserializeAs(Name = "origfilename")]
        public string FileName { get; set; }

        public DateTime ResourceDate { get; set; }
        public DateTime UploadedTime { get; set; }
        public DateTime ReviewDate { get; set; }
        public string Printing { get; set; }
        public Int32 Version { get; set; }
        public string Annotations { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public List<String> KeyWords { get; set; }

        [DeserializeAs(Name = "colorspace")]
        public string ColourSpace { get; set; }
        public string Owner { get; set; }
        public string UsageDetails { get; set; }
        public List<ResourceVersionModel> Versions { get; set; }

        [DeserializeAs(Name = "publicUrl")]
        public string PublicUrl { get; set; }

    }

    public class ResourceEstimatedSize
    {
        [DeserializeAs(Name = "estimatedFileSize")]
        public string FileSize { get; set; }
    }

    public class ResourceVersionModel
    {
        public string VersionNumber { get; set; }
        
        [DeserializeAs(Name = "origfilename")]
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }

        [DeserializeAs(Name = "datecreated")]
        public DateTime CreationDate { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
    }
}

