﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelligenceBank.ProofOfConcept.Models.RestClient
{
    public class PresetsModel
    {
        public string Message { get; set; }

        [DeserializeAs(Name = "presets")]
        public List<PresetModel> Presets { get; set; }
    }

    public class PresetModel
    {
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string FileType { get; set; }
        public string CompressionQuality { get; set; }
        public string CompressionType { get; set; }
        public string FileSize { get; set; }
        public string CustomWidth { get; set; }
        public string CustomHeight { get; set; }
        public string CropWidth { get; set; }
        public string CropHeight { get; set; }
        public string Id { get; set; }
    }
}
