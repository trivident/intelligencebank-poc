﻿namespace IntelligenceBank.ProofOfConcept.Models.RestClient
{
    public class LoginResponse
    {
        public int ClientId { get; set; }
       
        public string ClientTest { get; set; }
        
        public string ClientUUId { get; set; }
        
        public string ApiKey { get; set; }
        
        public int LoginTimeoutPeriod { get; set; }
        
        public string AdminEmail { get; set; }
        
        public string PasswordExpiryPeriod { get; set; }
        
        public string UserVerificationPeriod { get; set; }
        
        public int OfflineExpiryPeriod { get; set; }
        
        public string Annotations { get; set; }
  
        public string Comments { get; set; }
        
        public string PasswordExpired { get; set; }
        
        public string EmailExpired { get; set; }
        
        public string Versions { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string UserUUId { get; set; }

        public string FolderIcon { get; set; }

        public string RowsOnPage { get; set; }

        public string DefaultFolderSortOrder { get; set; }

        public string Message { get; set; }
    }
}
