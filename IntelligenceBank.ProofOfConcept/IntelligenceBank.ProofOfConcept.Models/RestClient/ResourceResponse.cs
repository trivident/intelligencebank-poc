﻿using System.IO;

namespace IntelligenceBank.ProofOfConcept.Models.RestClient
{
    public class ResourceResponse
    {
        public Stream ResourceStream { get; set; }
    }
}
