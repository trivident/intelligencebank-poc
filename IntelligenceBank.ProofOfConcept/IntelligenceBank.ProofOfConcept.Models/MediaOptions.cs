﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class IBMediaOptions
    {
        public string CropX { get; set; }
        public string CropY { get; set; }
        public string CropW { get; set; }
        public string CropH { get; set; }
        public string CopressionType { get; set; }
        public string CopressionQuality { get; set; }
        public string Size { get; set; }
    }
}