﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IntelligenceBank.ProofOfConcept.Models
{
    public enum CompressionType
    {
        [Display(Name = "No Compression")]
        NoCompression = 1,

        [Display(Name = "BZIP2", Description = "Lossless Compression")]
        BZIP2 = 2,

        [Display(Name = "JPEG", Description = "Lossy Compression")]
        JPEGLossy = 8,

        [Display(Name = "JPEG", Description = "Lossless Compression")]
        JPEG = 10,

        [Display(Name = "LZW", Description = "Lossless Compression")]
        LZW = 11,

        [Display(Name = "RunLengthEncoding", Description = "Lossless Compression")]
        RunLengthEncoding = 12,

        [Display(Name = "ZIP", Description = "Lossless Compression")]
        ZIP = 13
    }
}