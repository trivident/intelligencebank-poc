﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IntelligenceBank.ProofOfConcept.Models
{
    public class Preset
    {
        public string ItemId { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public string FileType { get; set; }
        public string Dimension { get; set; }
        public string CustomWidth { get; set; }
        public string CustomHeight { get; set; }
        public string CompressionType { get; set; }
        public string CompressionQuality { get; set; }
        public string CropX { get; set; }
        public string CropY { get; set; }
        public string CropWidth { get; set; }
        public string CropHeight { get; set; }

        //public void Map(Item item)
        //{
        //    if (item == null)
        //        throw new ArgumentNullException("item");

        //    ItemId = item.ID.ToString();
        //    FileType = item["FileType"];
        //    Size = item["Size"];
        //    CustomWidth = item["CustomWidth"];
        //    CustomHeight = item["CustomHeight"];
        //    CompressionType = item["CompressionType"];
        //    CompressionQuality = item["CompressionQuality"];
        //    CropX = item["CropX"];
        //    CropY = item["CropY"];
        //    CropWidth = item["CropWidth"];
        //    CropHeight = item["CropHeight"];
        //    Name = item["Name"];

        //    SetDefaults();
        //}

        private void SetDefaults()
        {
            if (string.IsNullOrEmpty(CropWidth))
                CropWidth = "0";

            if (string.IsNullOrEmpty(CropHeight))
                CropHeight = "0";
        }

        public string GetDescription()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine(string.Format("<b>{0}</b> <br/>", GetNameWithCropping()));
            stringBuilder.AppendLine(string.Format("Compression Quality: {0} <br/>", GetCompressionQuality()));
            stringBuilder.AppendLine(string.Format("File Type: {0} <br/>", FileType.ToUpper()));
            stringBuilder.AppendLine(string.Format("Dimensions: {0}", GetSize()));

            return stringBuilder.ToString();
        }
        
        private string GetCompressionQuality()
        {
            int quality = Convert.ToInt32(CompressionQuality);
            CompressionQuality type = (CompressionQuality)quality;
            var field = typeof(CompressionQuality).GetField((type.ToString()));
            var customAttribute = (DisplayAttribute)Attribute.GetCustomAttribute(field, typeof(DisplayAttribute));

            return string.Format("{0} ({1})", customAttribute.Description, customAttribute.Name);
        }

        private string GetSize()
        {
        //    var downloadSize = DownloadOptionsManager.Sizes.Get(Size);

            //return downloadSize.Value == 0 ? string.Format("{0} x {1}", CustomWidth, CustomHeight) : downloadSize.Title;
            throw new NotImplementedException("TODO");
        }

        private string GetNameWithCropping()
        {
            if (!string.IsNullOrEmpty(CropWidth) && !string.IsNullOrEmpty(CropHeight) && Convert.ToInt32(CropWidth) != 0 & Convert.ToInt32(CropHeight) != 0)
                return string.Format("{0} ({1} x {2} cropped)", Name, CropWidth, CropHeight);

            return Name;
        }
    }
}