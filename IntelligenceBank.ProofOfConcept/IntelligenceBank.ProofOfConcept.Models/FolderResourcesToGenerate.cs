﻿namespace IntelligenceBank.ProofOfConcept.Models
{
    public class FolderResourcesToGenerate
    {
        public Folder ParentFolder { get; set; }
        public FolderContent FolderContent { get; set; }
    }
}
